Source: entangle
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Matteo F. Vescovi <mfv@debian.org>
Section: utils
Priority: optional
Build-Depends: appstream,
               debhelper-compat (= 13),
               meson,
               python3,
               python3-pkg-resources,
               gobject-introspection,
               gtk-doc-tools,
               intltool,
               itstool,
               libgdk-pixbuf2.0-dev,
               libgexiv2-dev,
               libglib2.0-dev,
               libgphoto2-dev,
               libgstreamer1.0-dev,
               libgstreamer-plugins-base1.0-dev,
               libgtk-3-dev,
               libgudev-1.0-dev,
               liblcms2-dev,
               libpeas-dev,
               libraw-dev,
               pkg-config,
               yelp-tools
Rules-Requires-Root: no
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/entangle
Vcs-Git: https://salsa.debian.org/debian-phototools-team/entangle.git
Homepage: https://entangle-photo.org/

Package: entangle
Architecture: any
Depends: gir1.2-entangle-0.1,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: adwaita-icon-theme
Enhances: digikam,
          gimp,
          gnumed-client
Description: Tethered Camera Control & Capture
 Entangle provides a graphical interface for "tethered shooting", aka
 taking photographs with a digital camera completely controlled from the
 computer.
 .
 Using Entangle is as easy as 1,2,3...
  * Connect camera
  * Launch Entangle
  * Shoot photos
 .
 With a sufficiently capable digital SLR camera Entangle allows:
  * Trigger the shutter from the computer
  * Live preview of scene before shooting
  * Automatic download and display of photos as they are shot
  * Control of all camera settings from computer

Package: gir1.2-entangle-0.1
Architecture: any
Section: introspection
Depends: entangle (= ${binary:Version}),
         ${misc:Depends},
         ${gir:Depends}
Description: GObject introspection data for entangle
 Entangle provides a graphical interface for "tethered shooting", aka
 taking photographs with a digital camera completely controlled from the
 computer.
 .
 This package can be used by other packages using the GIRepository format
 to generate dynamic bindings.
