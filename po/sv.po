# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Translators:
# Göran Uddeborg <goeran@uddeborg.se>, 2012-2013, 2020.
# Klap <jojje3000@hotmail.com>, 2014
# Ulrika Uddeborg <aurorauddeborg@gmail.com>, 2014
# Göran Uddeborg <goeran@uddeborg.se>, 2015. #zanata, 2020.
# Göran Uddeborg <goeran@uddeborg.se>, 2016. #zanata, 2020.
# Göran Uddeborg <goeran@uddeborg.se>, 2018. #zanata, 2020.
msgid ""
msgstr ""
"Project-Id-Version: entangle\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-03 22:07+0100\n"
"PO-Revision-Date: 2020-01-13 17:15+0000\n"
"Last-Translator: Göran Uddeborg <goeran@uddeborg.se>\n"
"Language-Team: Swedish <https://translate.stg.fedoraproject.org/projects/"
"entangle/master/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.10.1\n"

msgid "'Escape' to close"
msgstr ""

msgid "0"
msgstr "0"

msgid "1.15:1 - Movietone"
msgstr "1.15:1 - Movietone"

msgid "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"
msgstr "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"

msgid "1.37:1 - 35mm movie"
msgstr "1.37:1 - 35mm film"

msgid "1.44:1 - IMAX"
msgstr "1.44:1 - IMAX"

msgid "1.50:1 (3:2, 15:10)- 35mm SLR"
msgstr "1.50:1 (3:2, 15:10)- 35mm SLR"

msgid "1.66:1 (5:3, 15:9) - Super 16mm"
msgstr "1.66:1 (5:3, 15:9) - Super 16mm"

msgid "1.6:1 (8:5, 16:10) - Widescreen"
msgstr "1.6:1 (8:5, 16:10) - Bredbild"

msgid "1.75:1 (7:4) - Widescreen"
msgstr "1.75:1 (7:4) - Bredbild"

msgid "1.77:1 (16:9) - APS-H / HDTV / Widescreen"
msgstr "1.77:1 (16:9) - APS-H / HDTV / Bredbild"

msgid "1.85:1 - 35mm Widescreen"
msgstr "1.85:1 - 35mm Bredbild"

msgid "12.00:1 - Circle-Vision 360"
msgstr "12.00:1 - Circle-Vision 360"

msgid "1:1 - Square / MF 6x6"
msgstr "1:1 - Kvadratisk / MF 6×6"

msgid "2.00:1 - SuperScope"
msgstr "2.00:1 - SuperScope"

msgid "2.10:1 (21:10) - Planned HDTV"
msgstr "2.10:1 (21:10) - Planerad HDTV"

msgid "2.20:1 (11:5, 22:10) - 70mm movie"
msgstr "2.20:1 (11:5, 22:10) - 70mm film"

msgid "2.35:1 - CinemaScope"
msgstr "2.35:1 - CinemaScope"

msgid "2.37:1 (64:27)- HDTV cinema"
msgstr "2.37:1 (64:27)- HDTV cinema"

msgid "2.39:1 (12:5)- Panavision"
msgstr "2.39:1 (12:5)- Panavision"

msgid "2.55:1 (23:9)- CinemaScope 55"
msgstr "2.55:1 (23:9)- CinemaScope 55"

msgid "2.59:1 (13:5)- Cinerama"
msgstr "2.59:1 (13:5)- Cinerama"

msgid "2.66:1 (8:3, 24:9)- Super 16mm"
msgstr "2.66:1 (8:3, 24:9)- Super 16mm"

msgid "2.76:1 (11:4) - Ultra Panavision"
msgstr "2.76:1 (11:4) - Ultra Panavision"

msgid "2.93:1 - MGM Camera 65"
msgstr "2.93:1 - MGM Camera 65"

msgid "3"
msgstr "3"

msgid "3:1 APS Panorama"
msgstr "3:1 APS Panorama"

msgid "4.00:1 - Polyvision"
msgstr "4.00:1 - Polyvision"

msgid "<b>Capture</b>"
msgstr "<b>Fångst</b>"

msgid "<b>Colour management</b>"
msgstr "<b>Färghantering</b>"

msgid "<b>Image Viewer</b>"
msgstr "<b>Bildvisare</b>"

msgid "<b>Interface</b>"
msgstr "<b>Gränssnitt</b>"

msgid "<b>Plugins</b>"
msgstr "<b>Insticksmoduler</b>"

msgid "About - Entangle"
msgstr "Om - Entangle"

msgid "Absolute colourimetric"
msgstr "Absolut kolorimetrisk"

msgid "All files (*.*)"
msgstr "Alla filer (*.*)"

msgid "Apply mask to alter aspect ratio"
msgstr "Använd mask för att ändra bildformat"

msgid "Aspect ratio:"
msgstr "Bildformat:"

msgctxt "shortcut window"
msgid "Autofocus"
msgstr "Autofokus"

#, c-format
msgid "Autofocus control not available with this camera"
msgstr "Autfokusinställning är inte tillgängligt för denna kamera"

#, c-format
msgid "Autofocus control was not a toggle widget"
msgstr "Autofokusinställningen var inte en omkopplings-widget"

msgid "Autofocus failed"
msgstr "Autofokus misslyckades"

msgid "Automatically connect to cameras at startup"
msgstr "Anslut automatiskt till kameror vid uppstart"

msgid "Automatically synchronize camera clock"
msgstr "Synkronisera automatiskt kameraklockan"

msgid "Automation"
msgstr "Automatisering"

msgid "Background:"
msgstr "Bakgrund:"

msgid "Best Fit"
msgstr "Bästa anpassning"

msgid "Blank screen when capturing images"
msgstr "Töm skärmen när bilder fångas"

msgid "Camera connect failed"
msgstr "Anslutning till kameran misslyckades"

msgid "Camera control update failed"
msgstr "Uppdatering av kamerainställningar misslyckades"

msgid "Camera is in use"
msgstr "Kameran används"

msgid "Camera load controls failed"
msgstr "Laddning av kamerainställningar misslyckades"

msgid "Cancel"
msgstr "Avbryt"

#, c-format
msgid "Cannot capture image while not opened"
msgstr ""

#, c-format
msgid "Cannot delete file while not opened"
msgstr ""

#, c-format
msgid "Cannot download file while not opened"
msgstr ""

#, c-format
msgid "Cannot initialize gphoto2 abilities"
msgstr "Kan inte initiera gphoto2:s egenskaper"

#, c-format
msgid "Cannot initialize gphoto2 ports"
msgstr "Kan inte initiera gphoto2:s portar"

#, c-format
msgid "Cannot load gphoto2 abilities"
msgstr "Kan inte ladda gphoto2:s egenskaper"

#, c-format
msgid "Cannot load gphoto2 ports"
msgstr "Kan inte ladda gphoto2:s portar"

#, c-format
msgid "Cannot preview image while not opened"
msgstr ""

#, c-format
msgid "Cannot wait for events while not opened"
msgstr ""

msgid "Capture"
msgstr "Fånga"

msgctxt "shortcut window"
msgid "Capture a single image"
msgstr "Fånga en enstaka bild"

msgid "Capture an image"
msgstr "Fånga en bild"

#, c-format
msgid "Capture target setting not available with this camera"
msgstr "Målfångstinställning ej kompatibel med denna kamera"

msgid "Capture;Camera;Tethered;Photo;"
msgstr "Capture;Camera;Tethered;Photo;Fångst;Kamera;Tjudrad;Foto;"

msgid "Center lines"
msgstr "Mittlinjer"

msgid ""
"Check that the camera is not\n"
"\n"
" - opened by another photo <b>application</b>\n"
" - mounted as a <b>filesystem</b> on the desktop\n"
" - in <b>sleep mode</b> to save battery power\n"
msgstr ""
"Kontrollera att kameran inte är\n"
"\n"
" - öppnad av ett annat foto<b>program</b>\n"
" - monterad som ett <b>filsystem</b> på skrivbordet\n"
" - i <b>sovläge</b> för att spara ström\n"

msgctxt "shortcut window"
msgid "Close all windows & exit"
msgstr "Stäng alla fönster och avsluta"

msgid "Color Management"
msgstr "Färghantering"

msgid "Colour managed display"
msgstr "Färghanterad display"

msgid "Continue preview mode after capture"
msgstr "Fortsätt i förhandsvisningsläge efter fångst"

msgid "Continuous capture preview"
msgstr "Kontinuerlig förhandsvisning av fångade bilder"

msgctxt "shortcut window"
msgid "Controlling the application"
msgstr "Styra programmet"

#, c-format
msgid "Controls not available for this camera"
msgstr "Kontroller är inte tillgängliga för denna kamera"

#, c-format
msgid "Controls not available when camera is closed"
msgstr ""

msgid "Delete"
msgstr "Radera"

msgid "Delete file from camera after downloading"
msgstr "Radera filer från kameran efter att de hämtats"

msgid "Detect the system monitor profile"
msgstr "Detektera systemmonitorprofilen"

msgid "Display focus point during preview"
msgstr "Visa fokuspunkt under förhandsvisning"

msgctxt "shortcut window"
msgid "Display help manual"
msgstr "Visa hjälpmanualen"

msgid "Entangle"
msgstr "Entangle"

msgid "Entangle Preferences"
msgstr "Inställningar för Entangle"

msgid ""
"Entangle can trigger the camera shutter to capture new images. When "
"supported by the camera, a continuously updating preview of the scene can be "
"displayed prior to capture. Images will be downloaded and displayed as they "
"are captured by the camera. Entangle also allows the settings of the camera "
"to be changed from the computer."
msgstr ""
"Entangle kan utlösa kameraslutaren för att ta nya bilder.  När kameran "
"stödjer det kan en kontinuerligt uppdaterad förhandsvisning av scenen visas "
"före kortet tas.  Bilkder kommer hämtas och visas allteftersom de tas av "
"kameran.  Entangle gör också att kamerainställningarna kan ändras från "
"datorn."

msgid ""
"Entangle is a program used to control digital cameras that are connected to "
"the computer via USB."
msgstr ""
"Entangle är ett program som används för att styra digitalkameror som är "
"anslutna till datorn via USB."

msgid ""
"Entangle is compatible with most DSLR cameras from Nikon and Canon, some of "
"their compact camera models, and a variety of cameras from other "
"manufacturers."
msgstr ""
"Entangle är kompatibel med de flesta DSLR-kameror från Nikon och Canon, "
"några av deras modeller av kompaktkameror, och olika kameror från andra "
"tillverkare."

msgid "Entangle: Camera autofocus failed"
msgstr "Entangle: Kamerans autofokus misslyckades"

msgid "Entangle: Camera connect failed"
msgstr "Entangle: Anslutning till kameran misslyckades"

msgid "Entangle: Camera control update failed"
msgstr "Entangle: Uppdatering av kamerainställningar misslyckades"

msgid "Entangle: Camera load controls failed"
msgstr "Entangle: Laddning av kamerainställningar misslyckades"

msgid "Entangle: Camera manual focus failed"
msgstr "Entangle: Kamerans manuella fokus misslyckades"

msgid "Entangle: Operation failed"
msgstr "Entangle: Åtgärden misslyckades"

#, c-format
msgid "Failed to read manual focus choice %d: %s %d"
msgstr "Misslyckades att läsa manuellt fokusval %d: %s %d"

#, c-format
msgid "Failed to set autofocus state: %s %d"
msgstr "Misslyckades att ställa in autofokustillstånd: %s %d"

#, c-format
msgid "Failed to set capture target: %s %d"
msgstr "Misslyckades att ställa in målfångst: %s %d"

#, c-format
msgid "Failed to set manual focus state: %s %d"
msgstr "Misslyckades att ställa in manuellt fokustillstånd: %s %d"

#, c-format
msgid "Failed to set time state: %s %d"
msgstr "Misslyckades att ställa in tidtillstånd: %s %d"

#, c-format
msgid "Failed to set viewfinder state: %s %d"
msgstr "Misslyckades att ställa in sökartillstånd: %s %d"

msgid "Filename pattern:"
msgstr "Filnamnsmönster:"

msgid "Flip horizontally"
msgstr ""

msgid "Flip vertically"
msgstr ""

msgctxt "shortcut window"
msgid "Focus control (during live preview)"
msgstr "Fokusstyrning (under förhandsvisning)"

msgctxt "shortcut window"
msgid "Focus in (large step)"
msgstr "Fokusera in (stort steg)"

msgctxt "shortcut window"
msgid "Focus in (small step)"
msgstr "Fokusera in (litet steg)"

msgctxt "shortcut window"
msgid "Focus out (large step)"
msgstr "Fokusera ut (stort steg)"

msgctxt "shortcut window"
msgid "Focus out (small step)"
msgstr "Fokusera ut (litet steg)"

msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Helskärm"

msgid "Golden sections"
msgstr "Gyllene snittet"

msgid "Grid lines:"
msgstr "Rutmönster:"

msgid "Highlight areas of overexposure"
msgstr "Markera områden med överexponering"

msgid "Highlight:"
msgstr "Markera:"

msgid "ICC profiles (*.icc, *.icm)"
msgstr "ICC-profiler (*.icc, *.icm)"

msgid "IP Address:"
msgstr "IP-adress:"

msgid "Image Viewer"
msgstr "Bildvisare"

msgctxt "shortcut window"
msgid "Image display"
msgstr "Bildvisning"

msgid "Image histogram"
msgstr "Bildhistogram"

msgid "Interface"
msgstr "Gränssnitt"

#, c-format
msgid "Manual focus control not available with this camera"
msgstr "Manuell fokusinställning är inte tillgängligt för denna kamera"

#, c-format
msgid "Manual focus control was not a range or radio widget"
msgstr ""
"Manuell fokusinställning var inte ett intervall eller en flervals-widget"

msgid "Manual focus failed"
msgstr "Manuellt fokus misslyckades"

msgid "Mask opacity:"
msgstr "Maskogenomskinlighet:"

msgid "Missing 'execute' method implementation"
msgstr "Implementationen av metoden ”execute” saknas"

msgid "Mode of operation:"
msgstr "Arbetsläge:"

msgid "Model"
msgstr "Model"

msgid "Monitor"
msgstr "Övervaka"

msgid "Monitor profile:"
msgstr "Monitorprofil:"

msgid "Network camera"
msgstr "Nätverkskamera"

msgid "No"
msgstr "Nej"

msgid "No camera connected"
msgstr "Ingen kamera ansluten"

msgid ""
"No cameras were detected, check that\n"
"\n"
"  - the <b>cables</b> are connected\n"
"  - the camera is <b>turned on</b>\n"
"  - the camera is in the <b>correct mode</b>\n"
"  - the camera is a <b>supported</b> model\n"
"\n"
"USB cameras are automatically detected\n"
"when plugged in, for others try a refresh\n"
"or enter a network camera IP address"
msgstr ""
"Inga kameror hittades, kontrollera att\n"
"\n"
"  - <b>sladdarna</b> är inkopplade\n"
"  - kameran är <b>påslagen</b>\n"
"  - kameran är i <b>rätt läge</b>\n"
"  - kameran är av en modell som <b>stödjs</b>\n"
"\n"
"USB-kameror upptäcks automatiskt när de kopplas\n"
"in, försök med en omläsning eller ange en\n"
"nätverkskameras IP-adress för övriga"

msgid "No config options"
msgstr "Inga konfigurationsflaggor"

msgid "No controls available"
msgstr "Inga inställningar tillgängliga"

msgid "No script"
msgstr "Inget skript"

msgid "None"
msgstr "Ingen"

msgid "Off"
msgstr "Av"

msgid "On"
msgstr "På"

msgid "Open"
msgstr "Öppna"

msgctxt "shortcut window"
msgid "Open a new camera window"
msgstr "Öppna ett nytt kamerafönster"

msgid "Open with"
msgstr "Öppna med"

#, c-format
msgid "Operation: %s"
msgstr "Åtgärd: %s"

msgid "Overlay earlier images"
msgstr "Täck över tidigare bilder"

msgid "Overlay layers:"
msgstr "Övertäckningslager:"

msgid "Pattern must contain 'XXXXXX' placeholder"
msgstr ""

msgid "Perceptual"
msgstr "Perceptuell"

msgid "Plugins"
msgstr "Insticksmoduler"

msgid "Port"
msgstr "Port"

msgctxt "shortcut window"
msgid "Presentation"
msgstr "Presentation"

msgid "Preview"
msgstr "Förhandsvisning"

msgid "Quarters"
msgstr "Fjärdedelar"

msgid "RGB profile:"
msgstr "RGB-profil:"

msgid "Relative colourimetric"
msgstr "Relativ kolorimetrisk"

msgid "Rendering intent:"
msgstr "Renderingssyfte:"

msgid "Reset controls"
msgstr "Återställ styrningen"

msgid "Retry"
msgstr "Försök igen"

msgid "Rule of 3rds"
msgstr "Tredjedelsregeln"

msgid "Rule of 5ths"
msgstr "Femtedelsregeln"

msgid "Saturation"
msgstr "Mättnad"

msgid "Screen blanking is not available on this display"
msgstr "Skärmsläckning är inte tillgängligt för denna skärm"

msgid "Screen blanking is not implemented on this platform"
msgstr "Skärmsläckning är inte implementerat på denna plattform"

msgid "Script"
msgstr "Skript"

msgid "Select a camera to connect to:"
msgstr "Välj en kamera att ansluta till:"

msgid "Select a folder"
msgstr "Välj en mapp"

msgid "Select application..."
msgstr "Välj program…"

msgid "Select camera - Entangle"
msgstr "Välj kamera - Entangle"

msgid "Set clock"
msgstr "Ställ klockan"

msgid "Show linear histogram"
msgstr "Visa linjärt histogram"

msgctxt "shortcut window"
msgid "Shutter control"
msgstr "Slutarstyrning"

msgid "Tear off"
msgstr ""

msgid "Tethered Camera Control & Capture"
msgstr "Tjudrad kamerakontroll och -fångst"

msgid "Tethered Camera Control &amp; Capture"
msgstr "Tjudrad kamerakontroll och -fångst"

msgid "The Entangle Photo project"
msgstr "Fotoprojektet Entagle"

msgid ""
"The camera cannot be opened because it is mounted as a filesystem. Do you "
"wish to umount it now?"
msgstr ""

msgid "This camera does not support image capture"
msgstr "Denna kamera stödjer inte fångst av bilder"

msgid "This camera does not support image preview"
msgstr "Denna kamera stödjer inte förhandsvisning av bilder"

#, c-format
msgid "Time setting not available with this camera"
msgstr "Tidsinställning ej kompatibel med denna kamera"

#, c-format
msgid "Time setting was not a choice widget"
msgstr "Tidsinställningen var inte en val-widget"

#, c-format
msgid "Time setting was not a date widget"
msgstr "Tidsinställningen var inte en datum-widget"

msgctxt "shortcut window"
msgid "Toggle histogram scale"
msgstr "Växla histogramskala"

msgctxt "shortcut window"
msgid "Toggle image mask"
msgstr "Växla bildmask"

msgctxt "shortcut window"
msgid "Toggle live preview"
msgstr "Växla live-förhandsvisning"

msgctxt "shortcut window"
msgid "Toggle overexposure highlighting"
msgstr "Växla överexponeringsmarkering"

#, c-format
msgid "Unable to capture image: %s"
msgstr "Kan inte fånga en bild: %s"

#, c-format
msgid "Unable to capture preview: %s"
msgstr "Kan inte fånga en förhandsvisning: %s"

#, c-format
msgid "Unable to connect to camera: %s"
msgstr "Kan inte ansluta till kameran: %s"

#, c-format
msgid "Unable to delete file: %s"
msgstr "Kan inte radera filen: %s"

#, c-format
msgid "Unable to fetch camera control configuration: %s"
msgstr "Kan inte hämta kamerans inställningskonfiguration: %s"

#, c-format
msgid "Unable to fetch widget name"
msgstr "Kan inte ta fram widget-namnet"

#, c-format
msgid "Unable to fetch widget type"
msgstr "Kan inte ta fram widget-typen"

#, c-format
msgid "Unable to get camera file: %s"
msgstr "Kan inte hämta fil från kameran: %s"

#, c-format
msgid "Unable to get file data: %s"
msgstr "Kan inte hämta fildata: %s"

#, c-format
msgid "Unable to get filename: %s"
msgstr "Kan inte hämta filnamnet: %s"

#, c-format
msgid "Unable to initialize camera: %s"
msgstr "Kan inte initiera kameran: %s"

#, c-format
msgid "Unable to load controls, camera is not opened"
msgstr ""

#, c-format
msgid "Unable to save camera control configuration: %s"
msgstr "Kan inte spara kamerans inställningskonfiguration: %s"

#, c-format
msgid "Unable to save controls, camera is not configurable"
msgstr "Kan inte spara inställningar, kameran är inte konfigurerbar"

#, c-format
msgid "Unable to save controls, camera is not opened"
msgstr ""

#, c-format
msgid "Unable to wait for events: %s"
msgstr "Kan inte vänta på händelser: %s"

msgid "Untitled script"
msgstr "Namnlöst skript"

msgid "Use embedded preview from raw files"
msgstr "Använd inbäddad förhandsvisning från råa filer"

msgid "Use preview output as capture image"
msgstr "Används förhandsvisningsutdata som fångstbild"

#, c-format
msgid "Viewfinder control not available with this camera"
msgstr "Sökarinställning är inte tillgängligt för denna kamera"

#, c-format
msgid "Viewfinder control was not a toggle widget"
msgstr "Sökarinställningen var inte en omkopplings-widget"

msgctxt "shortcut window"
msgid "Window display"
msgstr "Fönstervisning"

msgid "Yes"
msgstr "Ja"

msgid "Zoom _In"
msgstr "Zooma _in"

msgid "Zoom _Out"
msgstr "Zooma _ut"

msgctxt "shortcut window"
msgid "Zoom best"
msgstr "Zooma bäst"

msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Zooma in"

msgctxt "shortcut window"
msgid "Zoom normal"
msgstr "Zooma normalt"

msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Zooma ut"

msgid "_About"
msgstr "_Om"

msgid "_Cancel"
msgstr "_Avbryt"

msgid "_Connect…"
msgstr "_Anslut …"

msgid "_Disconnect…"
msgstr "_Koppla ner …"

msgid "_Fullscreen"
msgstr "_Helskärm"

msgid "_Help"
msgstr "_Hjälp"

msgid "_Keyboard shortcuts"
msgstr "_Tangentbordsgenvägar"

msgid "_Manual"
msgstr "_Manual"

msgid "_New window"
msgstr "_Nytt fönster"

msgid "_Normal Size"
msgstr "_Normal storlek"

msgid "_Open"
msgstr "_Öppna"

msgid "_Preferences…"
msgstr "_Inställningar …"

msgid "_Presentation"
msgstr "_Presentation"

msgid "_Quit"
msgstr "_Avsluta"

msgid "_Select session…"
msgstr "_Välj session …"

msgid "_Settings"
msgstr "_Inställningar"

msgid "_Supported cameras"
msgstr "Kameror som _stödjs"

msgid "_Synchronize capture"
msgstr "_Synkronisera fångst"

msgid "capture"
msgstr "fånga"

msgid "label"
msgstr "etikett"

msgid "preview"
msgstr "förhandsvisa"

msgid "settings"
msgstr "inställningar"

#~ msgid ""
#~ "The camera cannot be opened because it is currently mounted as a "
#~ "filesystem. Do you wish to umount it now ?"
#~ msgstr ""
#~ "Kameran kan inte öppnas för att den just nu är monterad som ett "
#~ "filsystem.  Vill du avmontera den nu?"

#~ msgid "Cannot capture image while not connected"
#~ msgstr "Kan inte fånga bilder utan anslutning"

#~ msgid "Cannot preview image while not connected"
#~ msgstr "Kan inte förhandsvisa en bild utan anslutning"

#~ msgid "Cannot download file while not connected"
#~ msgstr "Kan inte hämta filen utan anslutning"

#~ msgid "Cannot delete file while not connected"
#~ msgstr "Kan inte radera en fil utan anslutning"

#~ msgid "Cannot wait for events while not connected"
#~ msgstr "Kan inte vänta på händelser utan anslutning"

#~ msgid "Unable to load controls, camera is not connected"
#~ msgstr "Kan inte ladda inställningar, kameran är inte ansluten"

#~ msgid "Unable to save controls, camera is not connected"
#~ msgstr "Kan inte spara inställningar, kameran är inte ansluten"

#~ msgid "Controls not available when camera is disconnected"
#~ msgstr "Kontroller är inte tillgängliga när kameran inte är ansluten"

#~ msgid "Require gobject introspection &gt;= 1.54"
#~ msgstr "Begär gobjekt-introspektion ≥ 1.54"

#~ msgid "Require GTK3 &gt;= 3.22"
#~ msgstr "Begär GTK3 ≥ 3.22"

#~ msgid "Fix dependency on libraw"
#~ msgstr "Fixa beroende på libraw"

#~ msgid "Fix variable name in photobox plugin"
#~ msgstr "Fixa variabelnamn i photobox-insticksmodulen"

#~ msgid "Document some missing keyboard shortcuts"
#~ msgstr "Dokumentera några saknade tangentbordsgenvägar"

#~ msgid "Fix upper bound in histogram to display clipped pixel"
#~ msgstr "Fixa övre gräns i histogram för att visa avhuggen bildpunkt"

#~ msgid "Refresh translations"
#~ msgstr "Fräscha upp översättningar"

#~ msgid "Option to highlight over exposed pixels in red"
#~ msgstr "Alternativ för att markera överexponerade bildpunkter i rött"

#~ msgid "Disable noisy compiler warning"
#~ msgstr "Avaktivera en störande kompilatorvarning"

#~ msgid "Remove use of deprecated application menu concept"
#~ msgstr "Ta bort användningen av undanbedda programmenykoncept"

#~ msgid "Fix image redraw when changing some settings"
#~ msgstr "Fixa bildomritningen vid ändring av några inställningar"

#~ msgid "Update mailing list address in appdaat"
#~ msgstr "Uppdatera adressen till e-postlistan i appdata"

#~ msgid "Add more fields to appdata content"
#~ msgstr "Lägg till flera fält till appdata-innehållet"

#~ msgid "Fix reference counting during window close"
#~ msgstr "Fixa referensräknaren när fönster stängs"

#~ msgid "Use correct API for destroying top level windows"
#~ msgstr "Använd rätt API för att förstöra toppnivåfönster"

#~ msgid "Fix unmounting of cameras with newer gvfs URI naming scheme"
#~ msgstr "Fixa avmontering av kameror med nyare gvfs-URI-namngivningsschema"

#~ msgid "Avoid out of bounds read of property values"
#~ msgstr "Undvik läsningar av egenskapsvärden utanför gränserna"

#~ msgid "Fix many memory leaks"
#~ msgstr "Fixa många minnesläckor"

#~ msgid "Workaround for combo boxes not displaying on Wayland"
#~ msgstr "Gå runt att combo-rutor inte visas i Wayland"

#~ msgid "Fix race condition in building enums"
#~ msgstr "Fixa race condition vid bygge av uppräkningar"

#~ msgid "Fix setting of gschema directory during startup"
#~ msgstr "Fixa inställning av gschema-katalog vid uppstart"

#~ msgid "Set env to ensure plugins can find introspection typelib"
#~ msgstr ""
#~ "Sätt miljön för att säkerställa att insticksmoduler kan hitta "
#~ "introspektion av typelib"

#~ msgid "entangle"
#~ msgstr "entangle"
